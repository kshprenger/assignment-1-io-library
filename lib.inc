section .text
%define SPACE 0x20
%define TAB 0x9
%define NEW_LINE 0xA
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

;Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop_length:
        cmp byte [rdi + rax], 0
        je .end_string_length
        inc rax
        jmp .loop_length
    .end_string_length:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length ; вычисление длины для rdx
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rdx, rdx
    xor rsi, rsi
    mov rax, rdi
    mov rcx, NEW_LINE
    push dx ; 0 - терминатор для строки
    .division:
        push dx ; будем записывать символы попарно
        div rcx
        add dl, '0'
        mov byte[rsp+1], dl
        xor rdx, rdx
        cmp rax, 0x0
        je .end_div
        div rcx
        add dl, '0'
        mov byte[rsp], dl
        xor rdx, rdx
        cmp rax, 0x0
        je .end_div
        jmp .division
    .end_div:
        cmp byte[rsp], 0
        je .odd
    .even: ; четное число сиволов -> просто выводим
        mov rdi, rsp
        call print_string
        jmp .clear
    .odd: ; если нечётное, а мы записывали dx -> первые 8 бит - нули -> пропускаем их
        mov rdi, rsp
        inc rdi
        call print_string
    .clear:
        pop dx
        cmp dx, 0
        jne .clear
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .negative
    jmp .positive
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
    .positive:
        jmp print_uint

;; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor rdx, rdx
    .equals_loop:
        mov dl, [rsi + rcx]
        mov dh, [rdi + rcx]
        cmp dl, 0
        je .check_second
        cmp dh, 0
        je .check_first
        cmp dl, dh
        jne .not_equals
        inc rcx
        jmp .equals_loop
    .check_first:
        cmp dl, 0
        je .equals
        jmp .not_equals
    .check_second:
        cmp dh, 0
        je .equals
        jmp .not_equals
    .equals:
        mov rax, 0x1
        ret
    .not_equals:
        xor rax,rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, 0
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
    push rdi ;сохраняем адрес буфера, чтобы в случае успеха выдать его в rax, так как rdi подвергнется инкременту
    push rsi ;аналогично поступим с rsi, только в этом случае будет производиться его декремент 
    cmp rsi, 1
    jle .error ; в случае, если длина буффера <= 1, то слово невозможно будет записать
    .loop_before: ; проходим пробелы до начала слова
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        cmp al, SPACE
        je .loop_before
        cmp al, TAB
        je .loop_before
        cmp al, NEW_LINE
        je .loop_before
        cmp al, 0
        je .error ; строка состоит только из пробельных символов => нет слов
    .loop_after: ; дошли до начала слова, на этом этапе мы уверены, что буфер >=2, при этом первый нужный символ готов к записи
        cmp al, SPACE ; слово меньше буфера
        je .write_zero
        cmp al, TAB
        je .write_zero
        cmp al, NEW_LINE
        je .write_zero
        cmp al, 0
        je .write_zero
        cmp rsi, 1 ; остался последний байт в буфере для 0 в конце слова
        je .check_end_word
        mov byte[rdi], al
        push rdi
        push rsi
        call read_char ; читаем следующий
        pop rsi
        pop rdi
        inc rdi ; и сдвигаем позицию для его записи
        dec rsi ; остаток запаса буфера уменьшаем на 1
        jmp .loop_after
    .check_end_word:
        cmp al, SPACE
        je .write_zero
        cmp al, TAB
        je .write_zero
        cmp al, NEW_LINE
        je .write_zero
        jmp .error ; буфер закончился а слово - нет
    .write_zero:
        xor rax, rax
        mov byte [rdi], 0 
        pop rdx ; сохранённая длина буфера->rdx
        sub rdx, rsi ; подсчёт числа символов в слове
        pop rax ; сохранённый адрес буфера->rax
        ret
    .error:
        mov rdx, 0 ; отсутствие слов или переполнение буфера => длина некорректна, поэтому она равна 0
        pop rax ; очистка сохранённых значений ввода
        pop rax
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    xor rcx, rcx
    mov r9, 10
    mov sil, byte[rdi+rcx]
    cmp sil, '0'
    jb .error
    cmp sil, '9'
    ja .error
    sub sil, '0'
    add rax, rsi
    inc rcx
    jmp .main

    .main:
    mov sil, byte[rdi+rcx]
    cmp sil, '0'
    jb .done
    cmp sil, '9'
    ja .done
    mul r9
    jc .error ; переполнился rax -> недопустимое число
    sub sil, '0'
    add rax, rsi
    jc .error ; переполнился rax -> недопустимое число
    inc rcx
    jmp .main
    
   .done:
      mov rdx, rcx
      ret
   .error:
    xor rax, rax
    xor rdx, rdx
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .negative

    .positive:
        call parse_uint
        ret

    .negative:
        inc rdi
        call parse_uint
        cmp rdx, 0 
        je .error
        inc rdx
        neg rax
        ret
    .error:
        ret
        
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp byte[rdi + rax], 0
        je .check_buff_end ; достигли конца строки -> проверяем, есть ли в буфере место для 0
        cmp rdx, 0 ; буфер оказался меньше строки (включая 0-терминатор)
        je  .no
        mov cl, byte[rdi + rax]
        mov byte[rsi + rax], cl
        inc rax
        dec rdx
        jmp .loop
    .check_buff_end:
        cmp rdx, 0
        je .no
        jmp .yes
    .no:
        xor rax, rax
        ret
    .yes:
        mov byte[rsi + rax], 0
        ret
